/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;

public class FinalExamQ7 {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();

        DB database = client.getDB("photosharing");
        DBCollection albums = database.getCollection("albums");
        DBCollection images = database.getCollection("images");

        DBCursor cur = images.find();
        while(cur.hasNext()) {
            int idImage = Integer.parseInt(cur.next().get("_id").toString());
            if (albums.findOne(new BasicDBObject("images",idImage)) == null) images.remove(new BasicDBObject("_id",idImage));
            //else System.out.print("image "+idImage+" NO orfe\n");
        }
        System.out.print("Finished removing images\n");
    }
}
