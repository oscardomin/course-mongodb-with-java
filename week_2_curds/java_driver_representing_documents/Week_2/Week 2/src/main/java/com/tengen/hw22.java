package com.tengen;

/**
 * Created by oscar on 19/08/2014.
 */

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class hw22 {
    public static void main(String[] args) throws UnknownHostException {
        DBCollection collection = createCollection();

        /*List<String> names = Arrays.asList("alice", "bobby", "cathy", "david", "ethan");
        for (String name : names) {
            collection.insert(new BasicDBObject("_id", name));
        }*/

        DBObject query = new BasicDBObject("type", "homework");

        long count = collection.count(query);
        System.out.println(count);

        System.out.println("\nFind all: ");
        DBCursor cursor = collection.find(query)
                                    .sort(new BasicDBObject("student_id", 1).append("score", 1));

        //scratch(collection);
        int prevStudentId = -1;
        while (cursor.hasNext()) {
            DBObject cur = cursor.next();
            if((Integer) cur.get("student_id") > prevStudentId) {
                collection.remove(new BasicDBObject("_id",cur.get("_id")));
                System.out.print("Borra "+cur.get("_id")+"\n");
            }
            prevStudentId = (Integer) cur.get("student_id");
            //System.out.println(cur);
        }
    }

    // these are all the statement I used throughout the lecture.
    private static void scratch(DBCollection collection) {
        collection.update(new BasicDBObject("_id", "alice"),
                new BasicDBObject("age", 24));

        collection.update(new BasicDBObject("_id", "alice"),
                new BasicDBObject("$set", new BasicDBObject("age", 24)));

        collection.update(new BasicDBObject("_id", "alice"),
                new BasicDBObject(new BasicDBObject("gender", "F")));

        collection.update(new BasicDBObject("_id", "frank"),
                new BasicDBObject("$set", new BasicDBObject("age", 24)), true, false);

        collection.update(new BasicDBObject(),
                new BasicDBObject("$set", new BasicDBObject("title", "Dr")), false, true);

        collection.remove(new BasicDBObject("_id", "frank"));
    }

    private static DBCollection createCollection() throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("students");
        DBCollection collection = db.getCollection("grades");
        //collection.drop();
        return collection;
    }

    private static void printCollection(final DBCollection collection) {
        DBCursor cursor = collection.find().sort(new BasicDBObject("_id", 1));
        try {
            while (cursor.hasNext()) {
                System.out.println(cursor.next());
            }
        } finally {
            cursor.close();
        }

    }
}
